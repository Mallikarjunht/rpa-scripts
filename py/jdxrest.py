import os
import json
import requests

url = 'https://rpas-dev.2adpro.com/rpa/metadata/variable/metadata_2/309'
headers = {'Content-type': 'application/json', 'Cookie' : os.environ['Cookie']}
response = requests.get(url,headers=headers)
envValue = os.environ['test.env']
print("Env value is: "+envValue)
file = open('response.json', 'w')
if(response.status_code == 200):
    file.write(json.dumps(response.json()))
else:
    file.write('Response Code returned '+str(response.status_code));