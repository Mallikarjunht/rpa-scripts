from threading import Thread
import regex_utils as utils
import file_operations as fo
import os
import subprocess
import json

class worker(Thread):

    def __init__(self, file_name, expected_value, operation, result):
        Thread.__init__(self)
        self.file_name = file_name
        self.expected_value = expected_value
        self.result = result
        self.operation = operation
        self._return = None

    def run(self):
        print("Running Thread with file: " + self.file_name)
        file_version = utils.find_version_from_file(self.file_name)
        indd_version_code = '10018'
        indd_version_message = ''
        resolution_code = '10018'
        resolution_message = ''
        node_count_code = '10018'
        node_count_message = ''
        colour_profile_code = '10018'
        colour_profile_message = ''
        font_check_code = '10018'
        font_check_message = ''
        missing_font = []
        try:
            if self.operation == 'indd_version':
                print("its at indd")
                os.system(r"%RPA_Path%exif/exiftool.exe -csv "+self.file_name+" >"+os.getcwd()+'/response_'+os.path.split(self.file_name)[1]+'.csv')
                print("completed")
                actual,result = fo.indesign_version_check(os.getcwd()+'/response_'+os.path.split(self.file_name)[1]+'.csv', str(self.expected_value))
                if result is True:
                    indd_version_code = '10000'
                    indd_version_message = 'SUCCESS'
                else:
                    indd_version_code = '10015'
                    indd_version_message = file_version+ ' : Please verify the VERSION of the INDD file '+self.file_name+' .Expected version - '+str(self.expected_value)+' but uploaded version -'+actual
            elif self.operation == 'colour_profile':
                print("its at colorProfile")
                os.system(r"%RPA_Path%exif/exiftool.exe -csv "+self.file_name+" >"+os.getcwd()+'/response_'+os.path.split(self.file_name)[1]+'.csv')
                print("completed")
                actual,result = fo.colour_profile_check(os.getcwd()+'/response_'+os.path.split(self.file_name)[1]+'.csv', str(self.expected_value))
                if result is True:
                    colour_profile_code = '10000'
                    colour_profile_message = 'SUCCESS'
                else:
                    colour_profile_code = '10020'
                    colour_profile_message = file_version+ ' :Please verify the COLOR PROFILE of the image file '+self.file_name+' .Expected COLOR PROFILE - '+str(self.expected_value)+' but uploaded COLOR PROFILE - '+actual
            elif self.operation == 'resolution':
                subprocess.run([r"%RPA_Path%ResolutionFinder/ResolutionFinder.exe", self.file_name, str(self.expected_value)])
                f = open('response_'+os.path.split(self.file_name)[1]+'.json', 'rb')
                result = json.load(f)
                f.close
                resolution_code = result['response']
                if resolution_code == '10000':
                    resolution_message = 'SUCCESS'
                elif resolution_code == '10016':
                    actual = result['actual']
                    resolution_message = file_version+ ' :Please verify the DPI of the image file -'+self.file_name+' .Expected dpi - less than '+str(self.expected_value)+' but uploaded DPI -'+actual
                else:
                    resolution_message = 'Not verified by the RPA.  Click ‘Ok’ to move to the next state or ‘Cancel’ to try again.'
            elif self.operation == 'node_count':
                subprocess.run([r"%RPA_Path%NodeFinder/IllustratorNodeFinder.exe", self.file_name, str(self.expected_value)])
                f = open('response_'+os.path.split(self.file_name)[1]+'.json', 'rb')
                result = json.load(f)
                f.close()
                node_count_code = result['response']
                actual = result['actual']
                if node_count_code == '10000':
                    node_count_message = 'SUCCESS'
                elif node_count_code == '10014':
                    node_count_message = file_version+ ' :Please verify the NODE counts in the file -'+self.file_name+'. Expected node count - less than'+str(self.expected_value)+' but uploaded node count - '+actual
                else:
                    node_count_message = 'Not verified by the RPA.  Click ‘Ok’ to move to the next state or ‘Cancel’ to try again.'
            elif self.operation == 'font_check':
                print("in font check")
                subprocess.run([r"%RPA_Path%MissingFontChecker/MissingFontChecker.exe", self.file_name, str(self.expected_value)])
                f = open('response_'+os.path.split(self.file_name)[1]+'.json', 'rb')
                result = json.load(f)
                f.close()
                font_check_code = result['response']
                missing_font = result['missingFonts']
                if font_check_code == '10000':
                    font_check_message = 'SUCCESS'
                elif font_check_code == '10021':
                    font_check_message = file_version+ ' :Please verify the FONTS/GLYPHS in the indd file -'+self.file_name+' .Expected FONTS/GLYPHS - '+str(self.expected_value)+' but uploaded FONTS/GLYPHS - '+str(missing_font)
                else:
                    font_check_message = 'Not verified by the RPA.  Click ‘Ok’ to move to the next state or ‘Cancel’ to try again.'
                print("font check completed")
            self.result = {'file_name':self.file_name, 'size_code': '10018', 'version_code': '10018', 'indd_version_code': indd_version_code, 'indd_version_message': indd_version_message, 'resolution_code': resolution_code, 'resolution_message': resolution_message, 'node_count_code': node_count_code, 'node_count_message': node_count_message,'colour_profile_code': colour_profile_code, 'colour_profile_message':colour_profile_message, 'font_check_code': font_check_code, 'font_check_message':font_check_message, 'missing_font': missing_font}
        except Exception as e:
            print("File is not present on the provided location or path " + self.file_name)
            print(e)
            if self.operation == 'indd_version':
                indd_version_code = '10012'
                indd_version_message = file_version+ ' :Please verify the uploaded file(s) - no file found.'
            elif self.operation == 'colour_profile':
                colour_profile_code = '10012'
                colour_profile_message = file_version+ ' :Please verify the uploaded file(s) - no file found.'
            elif self.operation == 'node_count':
                node_count_code = '10012'
                node_count_message = file_version+ ' :Please verify the uploaded file(s) - no file found.'
            elif self.operation == 'resolution':
                resolution_code = '10012'
                resolution_message = file_version+ ' :Please verify the uploaded file(s) - no file found.'
            elif self.operation == 'font_check':
                font_check_code = '10012'
                font_check_message = file_version+ ' :Please verify the uploaded file(s) - no file found.'
            self.result = {'file_name':self.file_name, 'size_code': '10018', 'version_code': '10018', 'indd_version_code': indd_version_code, 'indd_version_message': indd_version_message, 'resolution_code': resolution_code, 'resolution_message': resolution_message, 'node_count_code': node_count_code, 'node_count_message': node_count_message,'colour_profile_code': colour_profile_code, 'colour_profile_message':colour_profile_message, 'font_check_code': font_check_code, 'font_check_message':font_check_message, 'missing_font': []}
            #self.result = {'file_name':self.file_name, 'version_code': '10018', 'size_code': '10018', 'indd_version_code': indd_version_code, 'resolution_code': resolution_code, 'node_count_code': node_count_code, 'node_count_message': node_count_message, 'colour_profile_code': colour_profile_code, 'font_check_code': font_check_code, 'missing_font': []}

    def join(self):
        Thread.join(self)
        return self.result