import json
import requests


def login():
    url = 'https://sso-qa.2adpro.com/sso/login'
    data = '{ "username" :"sriram.s", "password" :"welcome@123" }'
    headers = {'Content-type': 'application/json'}
    response = requests.post(url, data=data, headers=headers)
    cookie = ''
    for c in response.cookies:
        cookie = cookie + c.name + ":" + c.value + "\n"

    fileName = 'E:\\tmp\\response.json'
    file = open(fileName, 'w')
    file.write(json.dumps(response.json()))


def adobe_uri( api_name, input_url, output_url, auth_key):
    url = 'https://image.adobe.io/lrService/' + api_name
    data = '{ 	"inputs": { 		"href": "' + input_url + '", 		"storage": "external" 	},	"outputs": [ 		{ 			"href": "' + output_url + '", 			"storage": "external", 			"type": "image/jpeg" 		}	]}'
    headers = {'Content-type': 'application/json',
               'x-api-key':'8138facda56840edafc6d97d5d1c0102',
               'Authorization': 'Bearer '+ auth_key
               }
    response = requests.post(url, data=data, headers=headers)
    return response

def adobe_uri_sensei( api_name, input_url, output_url, auth_key):
    url = 'https://image.adobe.io/sensei/cutout'
    data = '{ 	"input": { 		"href": "' + input_url + '", 		"storage": "external" 	},	"output": { "href": "' + output_url + '", 			"storage": "external", 			"mask": {"format":"soft"} 	}}'
    headers = {'Content-type': 'application/json',
               'x-api-key':'8138facda56840edafc6d97d5d1c0102',
               'Authorization': 'Bearer '+ auth_key
               }
    response = requests.post(url, data=data, headers=headers)
    return response