import json
import sys
import requests




def get_cc_count(pubId, corpId, width, depth, level, revision_num, advertiser_name, from_status, to_status):
    url = "https://rpacc-qa.2adpro.com/cc_automation_app/upload/"
    headers = {'Content-type': 'application/json'}
    body = {"data":[{"jom_pub_id":pubId, "jom_corp_id":corpId, "jom_width":width, "jom_depth":depth, "jjl_design_level":level, "jjsc_rev_cycle":revision_num, "jom_adv_name":advertiser_name, "jjsc_from_status":from_status, "jjsc_to_status":to_status}]}
    response = requests.post(url, headers=headers, json=body)
    cc_file = open('response.json', 'w')
    print("responce "+ response.text)
    if response.status_code == 200 or response.status_code == 201:
        print("Data =>" +response.text)
        cc_file.write(json.dumps(response.json()))
    else:
        print("ERROR => " + response.text)
        cc_file.write(json.dumps(response.json()))
    cc_file.close()

pubId = int(sys.argv[1])
corpId = int(sys.argv[2])
width = float(sys.argv[3])
depth = float(sys.argv[4])
level = float(sys.argv[5])
revision_num = int(sys.argv[6])
advertiser_name = sys.argv[7]
from_status = sys.argv[8]
to_status = sys.argv[9]

print(sys.argv)
get_cc_count(pubId, corpId, width, depth, level, revision_num, advertiser_name, from_status, to_status)


# 2098, 53,8.22, 0.14, 4.00, 0, "ACE HARDWARE-MARTINSVI", "Q", "S"