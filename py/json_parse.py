import re
import os
import requests

def parse_and_get(variable_name, key, corp_id):
    
    url=os.environ["env.rpa.url"]+"metadata/variable/"+variable_name+"/"+corp_id
    headers = {'Content-type': 'application/json', 'Cookie': os.environ['Cookie']}
    response = requests.get(url, headers=headers)
    
    if response.status_code == 200 or response.status_code == 202:
        json_data = response.json()
    elif response.status_code == 220:
        raise Exception("9002")
    else:
        raise Exception("9003")

    try:
        split = key.split('.')
        json_data_iterator = json_data
        for nestedData in split:
            if nestedData.startswith('['):
                match = re.search(r"\[([A-Za-z0-9_,]+)\]",nestedData)
                search = match.group(1).split(',')
                for arrayData in json_data_iterator:
                    if str(arrayData[search[0]]) == search[1]:
                        json_data_iterator = arrayData
                        break
            else:
                json_data_iterator = json_data_iterator[nestedData]
    except Exception as e:
        raise Exception("9004")
    
    return json_data_iterator