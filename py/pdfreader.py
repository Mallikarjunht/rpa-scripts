import sys
import json_parse as json_parse
import filedownloadfromS3 as filedownload
import pdf_version_compare as pdfCompare

variable_name = sys.argv[1]
corp_id = sys.argv[2]
key = sys.argv[3]
input_file = sys.argv[4]

pdf_version = json_parse.__parse_and_get(variable_name,corp_id,key)
download_status = filedownload.download_file(input_file)
status_file = open('response.json', 'w')
if(download_status == False):
    status_file.write('{"response" : "Failed", "reason" : "File Download Failed"}')
else:
    version=1.5
    if(pdfCompare.version_compare(input_file,1.5)):
        status_file.write('{"response" : "Success"}')
    else:
        status_file.write('{"response" : "Failed", "reason" : "Version Dependency not satisfied"}')