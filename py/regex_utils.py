import re

def find_version_from_file(file):
    val = file.replace('\\','/')
    m = re.search(r'/(V[0-9]+)/',val)
    if m:
        found = m.group(0)
        print(found[1:-1])
        return found[1:-1]
    else:
        return 'V1'

if __name__ == "__main__":
    val = find_version_from_file('E:/2adpro/workflow/rpa-scripts/py/8000841/output/print/aV21/R0/Testorder23/AD-00272372.indd')
    print(val)
