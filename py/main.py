import sys
import os
import json
import json_parse as json_parse
import pdf_version_worker_thread as pdfThread
import job_thread as jobThread
from zipfile import ZipFile
import regex_utils as utils

if __name__ == "__main__":

    variable_name = sys.argv[1]
    corp_id = sys.argv[2]
    keys = sys.argv[3]
    input_file = sys.argv[4]
    general_corp_id = sys.argv[5]
    ip_address = sys.argv[6]
    workflow_ip = sys.argv[7]

    if workflow_ip == '172.16.12.12':
        server = 'Y:/'
    else:
        server = 'Z:/'
    status_file = open('response.json', 'w')

    try:
        output_formats = json_parse.parse_and_get(variable_name, corp_id+'.outputFormat', general_corp_id).split(',')

        key_list = keys.split(',')
        for key in key_list:
            if 'pdfVersion' in key:
                pdf_version = json_parse.parse_and_get(variable_name,key, general_corp_id)
            elif 'pdfFileSize' in key:
                pdf_size = json_parse.parse_and_get(variable_name, key, general_corp_id)
            elif 'inddVersion' in key:
                indd_version = json_parse.parse_and_get(variable_name, key, general_corp_id)
            elif 'dpi' in key:
                if 'newsPrint' in key:
                    resolution = json_parse.parse_and_get(variable_name, key, general_corp_id)
                elif 'magazine' in key:
                    resolution = json_parse.parse_and_get(variable_name, key, general_corp_id)
            elif 'nodes' in key:
                node_count = json_parse.parse_and_get(variable_name, key, general_corp_id)
            elif 'colorprofile' in key:
                color_profile = json_parse.parse_and_get(variable_name, key, general_corp_id)
            elif 'fonts' in key:
                font_profiles = json_parse.parse_and_get(variable_name, key, general_corp_id)
        input_files = input_file.split(',')
        result_list = list()
        thread_list = list()
        tmp_list = list()
        for j in input_files:
            j = server+ip_address+"/"+j
            tmp_list.append(j)
        input_files = tmp_list
        
        for i in input_files:
            file_format = os.path.splitext(i)[1]
            if file_format == '.zip':
                try:
                    print("zip start")
                    with ZipFile(i, 'r') as zipObj:
                        print("opening zip")
                        listOfFileNames = zipObj.namelist()
                        zip_path = '/'.join(i.split('/')[2:]).split('.')[0]
                        print(listOfFileNames)
                        print(zip_path)
                        for fileName in listOfFileNames:
                            if os.path.splitext(fileName)[1] in output_formats and os.path.splitext(fileName)[1] != '.pdf':
                                zipObj.extract(fileName, zip_path)
                                input_files.append(os.getcwd()+"/"+zip_path+"/"+fileName)
                        print(input_files)
                    continue
                except Exception as e:
                    print("Error with Extracting Zip File")
                    print(e)
                    result_list.append({'file_name': i, 'zip_error': '10019', 'version_code': '10018', 'size_code': '10018', 'indd_version_code': '10018', 'resolution_code': '10018', 'node_count_code': '10018', 'colour_profile_code': '10018', 'font_check_code': '10018'})
                    continue
                    
            if file_format in output_formats:
                try:
                    if file_format == '.pdf' and 'pdf_version' in vars() and 'pdf_size' in vars():
                        thread = pdfThread.PDFWorker(i, pdf_version, pdf_size, result=None)
                        thread_list.append(thread)
                    elif file_format == '.indd':
                        if 'indd_version' in vars():
                            thread = jobThread.worker(i, indd_version, 'indd_version', result=None)
                            thread_list.append(thread)
                        if 'font_profiles' in vars():
                            print("font check thread")
                            thread_secondary = jobThread.worker(i, font_profiles, 'font_check', result=None)
                            thread_list.append(thread_secondary)
                    elif file_format in ['.jpeg', '.jpg', '.png', '.tiff', '.tif', '.psd']:
                        if 'resolution' in vars():
                            thread = jobThread.worker(i, resolution, 'resolution', result=None)
                            thread_list.append(thread)
                        if file_format in ['.jpeg', '.jpg', '.png'] and 'color_profile' in vars():
                            print("adding colourProfile thread")
                            thread_secondary = jobThread.worker(i, color_profile, 'colour_profile', result=None)
                            thread_list.append(thread_secondary)
                    elif file_format in [".ai", ".eps"] and 'node_count' in vars():
                        thread = jobThread.worker(i, node_count, 'node_count', result=None)
                        thread_list.append(thread)
                except NameError:
                    continue
            else:
                print("file format not supported")
                result_list.append({'file_name': i, 'version_code': '10017', 'size_code': '10017', 'indd_version_code': '10017', 'resolution_code': '10017', 'node_count_code': '10017', 'colour_profile_code': '10017', 'font_check_code': '10017'})
            
        for thread in thread_list:
            thread.start()
            result_list.append(thread.join())

        print(result_list)
        response_json_str = '{"response": { "pdf_version": { "success": [], "failed": [] }, "general": { "success": [], "failed": [] }, "resolution": { "success": [], "failed": [] }, "missingFontCheck": { "success": [], "failed": [] }, "node_count": { "success": [], "failed": [] }, "indd_version": { "success": [], "failed": [] }, "colour_profile": { "success": [], "failed": [] }, "pdf_size": { "success": [], "failed": [] } } }'
        response_json = json.loads(response_json_str)
        for result_dict in result_list:
            if result_dict['size_code'] == '10000':
                response_json['response']['pdf_size']['success'].append(result_dict['file_name'])
            elif result_dict['size_code'] != '10018':
                failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['size_code'],'errorMessage':result_dict['size_message']}
                response_json['response']['pdf_size']['failed'].append(failed_response_dict)

            if result_dict['version_code'] == '10000':
                response_json['response']['pdf_version']['success'].append(result_dict['file_name'])
            elif result_dict['version_code'] != '10018':
                failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['version_code'],'errorMessage':result_dict['version_message']}
                response_json['response']['pdf_version']['failed'].append(failed_response_dict)
                
            if result_dict['indd_version_code'] == '10000':
                response_json['response']['indd_version']['success'].append(result_dict['file_name'])
            elif result_dict['indd_version_code'] != '10018':
                failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['indd_version_code'], 'errorMessage':result_dict['indd_version_message']}
                response_json['response']['indd_version']['failed'].append(failed_response_dict)
                
            if result_dict['colour_profile_code'] == '10000':
                response_json['response']['colour_profile']['success'].append(result_dict['file_name'])
            elif result_dict['colour_profile_code'] != '10018':
                failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['colour_profile_code'], 'errorMessage':result_dict['colour_profile_message']}
                response_json['response']['colour_profile']['failed'].append(failed_response_dict)
                
            if result_dict['resolution_code'] == '10000':
                response_json['response']['resolution']['success'].append(result_dict['file_name'])
            elif result_dict['resolution_code'] != '10018':
                failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['resolution_code'], 'errorMessage':result_dict['resolution_message']}
                response_json['response']['resolution']['failed'].append(failed_response_dict)
                
            if result_dict['node_count_code'] == '10000':
                response_json['response']['node_count']['success'].append(result_dict['file_name'])
            elif result_dict['node_count_code'] != '10018':
                failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['node_count_code'], 'errorMessage':result_dict['node_count_message']}
                response_json['response']['node_count']['failed'].append(failed_response_dict)

            if result_dict['font_check_code'] == '10000':
                response_json['response']['missingFontCheck']['success'].append(result_dict['file_name'])
            elif result_dict['font_check_code'] != '10018':
                failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['font_check_code'],'missingFonts':result_dict['missing_font'],  'errorMessage':result_dict['font_check_message']}
                response_json['response']['missingFontCheck']['failed'].append(failed_response_dict)
            
            try:
                if result_dict['zip_error'] == '10019':
                    file_version = utils.find_version_from_file(result_dict['file_name'])
                    error_message = file_version+ ' :Please check the zip file '+result_dict['file_name']
                    failed_response_dict = {'fileName':result_dict['file_name'],'errorCode':result_dict['zip_error'], 'errorMessage':error_message}
                    response_json['response']['general']['failed'].append(failed_response_dict)
            except:
                continue

        status_file.write(json.dumps(response_json))
        
    except Exception as e:
        print(e)
        status_file.write('{"response" : "Failed", "reason" : "'+str(e)+'", "errorMessage": "Not verified by the RPA.  Click Ok to move to the next state or Cancel to try again." }')