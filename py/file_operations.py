import re
import os
import sys
from PIL import Image
import csv
import json_parse as json_parse
import filedownloadfromS3 as filedownload
from PyPDF2.pdf import PdfFileReader

def pdf_version_compare(input_file, expected_version):
    file = open(input_file, 'rb')
    doc = PdfFileReader(file)
    doc.stream.seek(0) # Necessary since the comment is ignored for the PDF analysis
    val = "".join(chr(x) for x in doc.stream.readline())
    version = re.findall("\d+\.\d+", val)
    file = open('response_1.json', 'w')
    if(float(version.__getitem__(0)) == expected_version):
        return [version.__getitem__(0), True]
    else:
        return [version.__getitem__(0), False]

def resolution_check(path, expected_resolution):
    extension = os.path.splitext(path)[1]
    if(extension == '.tiff' or extension == '.tif' or extension == '.psd'):
        return exif_data(path, expected_resolution)
    else:
        image_file = Image.open(path)
        if image_file.info.get('dpi'):
            x_dpi = image_file.info['dpi'][0]
            # print(x_dpi)
            if(int(x_dpi) == int(expected_resolution)):
                return [x_dpi, True]
            else:
                return [x_dpi, False]

def size_check(path, expected_value):
    size = os.stat(path).st_size/1000000
    if(size > 0 and size <= float(expected_value)):
        return [str(size)+"MB", True]
    else:
        return [str(size)+"MB", False]

def indesign_version_check(file_name, expected_version):
    with open(file_name, 'rt') as f:
        reader = csv.DictReader(f)
        for row in reader:
            version = row['CreatorTool'].split()[3]
            if (float(version) == float(expected_version)):
                return [expected_version,True]
            else:
                return [expected_version,False]
            
def colour_profile_check(file_name, expected_value):
    with open(file_name, 'rt') as f:
        reader = csv.DictReader(f)
        for row in reader:
            try:
                profile_name = row['ICCProfileName']
                if (profile_name == expected_value):
                    return [profile_name,True]
                else:
                    return [profile_name,False]
            except Exception as e:
                print("colorProfile:"+str(e))
                return False

def version_tuple(v):
    return tuple(map(int, (v.split("."))))

def exif_data(path, expected_resolution):
    # xmp = file_to_dict(path)
    # # print(xmp)
    # dc = xmp["http://ns.adobe.com/tiff/1.0/"]
    # # print(dc)
    # dpi = 0
    # for i in dc:
    #     if i[0] == "tiff:XResolution":
    #         a, b = i[1].split('/')
    #         dpi = int(a)/int(b)
    #         # print(dpi)
    # if(dpi == 0):
    #     return None
    # if(dpi == int(expected_resolution)):
    #     return [dpi, True]
    # else:
    #     return [dpi, False]
    return None

def get_size(value):
    size = value[:-2]
    unit = value[-2:].lower()
    if(unit == 'kb'):
        return float(size/1000)
    elif(unit == 'mb'):
        return float(size)
    elif(unit == 'gb'):
        return float(size*1000)

if __name__ == "__main__":
    task = sys.argv[1]
    path = sys.argv[2]
    variable_name = sys.argv[3]
    corp_id = sys.argv[4]
    expected_value = json_parse.parse_and_get(variable_name, corp_id)
    download_status = filedownload.download_file(path)
    status_file = open('response.json', 'w')
    if(download_status == False):
        status_file.write('{"response" : "Failed", "reason" : "File does not exist in bucket"}')
    else:
        if(task == "res"):
            res, flag = resolution_check(path, expected_value)
            if(flag):
                status_file.write('{"response" : "Success", "resolution": "'+ str(res) +'"}')
            else:
                status_file.write('{"response" : "Failed", "reason" : "File Resolution Exceded Limit", "resolution": "'+ str(res) +'"}')
        elif(task == "size"):
            size, flag = size_check(path, get_size(expected_value))
            if(flag):
                status_file.write('{"response" : "Success", "fileSize": "'+ str(size) +'"}')
            else:
                status_file.write('{"response" : "Failed", "reason" : "File Size Exceded Limit", "fileSize": "'+ str(size) +'"}')
        elif(task == "adobe"):
            version, flag = indesign_version_check(path, expected_value)
            if(flag):
                status_file.write('{"response" : "Success", "version": "'+ str(version) +'"}')
            else:
                status_file.write('{"response" : "Failed", "reason" : "Adobe Version Dependency not satisfied", "version": "'+ str(version) +'"}')
        elif(task == "version"):
            version,flag = pdf_version_compare(path, expected_value)
            if(flag):
                status_file.write('{"response" : "Success", "version": "'+ str(version) +'"}')
            else:
                status_file.write('{"response" : "Failed", "reason" : "Version Dependency not satisfied", "version": "'+ str(version) +'"}')