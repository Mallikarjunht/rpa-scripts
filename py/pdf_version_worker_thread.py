from threading import Thread
import os
import file_operations as fo
import regex_utils as utils


class PDFWorker(Thread):

    def __init__(self, file_name, expected_version, expected_size, result):
        Thread.__init__(self)
        self.file_name = file_name
        self.expected_version = expected_version
        self.expected_size = expected_size
        self.result = result
        self._return = None

    def run(self):
        print("Running Thread with file: " + self.file_name)
        file_version = utils.find_version_from_file(self.file_name)
        try:
            version, result = fo.pdf_version_compare(self.file_name, self.expected_version)
            # result = pdfCompare.version_compare(self.file_name, self.expected_version)
            size = os.path.getsize(filename=self.file_name)
            if size <= int(self.expected_size):
                size_code = '10000'
                size_message = 'SUCCESS'
            else:
                size_code = '10013'
                size_message = file_version + ': Please verify the SIZE of the pdf file. Expected file size - less than ' + str(
                    self.expected_size) + ' but uploaded file size ' + str(size)
            if result is True:
                version_code = '10000'
                version_message = 'SUCCESS'
            else:
                version_code = '10011'
                version_message = file_version + ': Please verify the VERSION of the pdf file - ' + self.file_name + '. Expected pdf version - ' + str(
                    self.expected_version) + '. But uploaded version - ' + str(version)

            self.result = {'file_name': self.file_name, 'size_code': size_code, 'version_code': version_code,
                           'size_message': size_message, 'version_message': version_message,
                           'indd_version_code': '10018', 'resolution_code': '10018', 'node_count_code': '10018',
                           'colour_profile_code': '10018', 'font_check_code': '10018'}
        except Exception as e:
            print("File is not present on the provided location or path " + self.file_name)
            print("pdf:" + str(e))
            size_message = file_version + ': Please verify the uploaded file(s) - no file found.'
            version_message = file_version + ': Please verify the uploaded file(s) - no file found.'
            self.result = {'file_name': self.file_name, 'version_code': '10012', 'size_code': '10012',
                           'size_message': size_message, 'version_message': version_message,
                           'indd_version_code': '10018', 'resolution_code': '10018', 'node_count_code': '10018',
                           'colour_profile_code': '10018', 'font_check_code': '10018'}

    def join(self):
        Thread.join(self)
        return self.result
