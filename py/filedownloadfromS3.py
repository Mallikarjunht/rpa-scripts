import boto3
import os
import requests

def download_file(input):
    s3client = boto3.client('s3', region_name=os.environ['AWS_REGION'])
    bucket_name = os.environ['bucket_name']
    input_url = s3client.generate_presigned_url(
        'get_object', {'Bucket': bucket_name, 'Key': input})
    r = requests.get(url=input_url)

    os.makedirs(os.path.dirname(input), exist_ok=True)
    with open(input, "wb") as f:
        f.write(r.content)
    if(os.path.exists(input)):
        return True;
    return False;